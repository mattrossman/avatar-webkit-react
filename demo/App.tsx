import { useEffect, useState } from 'react'
import { Environment, OrbitControls } from '@react-three/drei'
import { Canvas } from '@react-three/fiber'
import { hallwayPublicCDNUrl } from '@quarkworks-inc/avatar-webkit-rendering'
import { useControls } from 'leva'

import { AUPredictorProviderDamped } from '../src/context/PredictorContext'
import { EmojiAvatar } from '../src/components/EmojiAvatar'
import { ReadyPlayerMeAvatar } from '../src/components/ReadyPlayerMeAvatar'
import { MozillaAvatar } from '../src/components/MozillaAvatar'

const CAMERA_WIDTH = 640
const CAMERA_HEIGHT = 480
const API_TOKEN = import.meta.env.VITE_AVATAR_WEBKIT_AUTH_TOKEN

export default function App() {
  const [stream, setStream] = useState<MediaStream>()

  const startVideo = async () => {
    const constraints = {
      width: CAMERA_WIDTH,
      height: CAMERA_HEIGHT,
    }
    const stream = await navigator.mediaDevices.getUserMedia({ video: constraints })
    setStream(stream)
  }

  useEffect(() => {
    startVideo()
  }, [])

  const { damping } = useControls({
    damping: { value: 0.35, min: 1e-9, max: 1 },
  })

  const lambda = 1 / Math.pow(damping, 4)

  return (
    <Canvas camera={{ fov: 30 }}>
      <AUPredictorProviderDamped
        lambda={lambda}
        running={true}
        stream={stream}
        apiToken={API_TOKEN}
        options={{ processWidth: CAMERA_WIDTH / 2, processHeight: CAMERA_HEIGHT / 2, shouldMirrorOutput: true }}
      >
        <OrbitControls />
        <Environment preset="warehouse" background />
        <ReadyPlayerMeAvatar url={hallwayPublicCDNUrl('models/hannah.glb')} position={[-1, 0, 0]} scale={2} />
        <EmojiAvatar eyeColor="black" faceColor="green" position={[0, 0, 0]} />
        <MozillaAvatar url={hallwayPublicCDNUrl('models/mozilla.glb')} position={[1, 0, 0]} />
      </AUPredictorProviderDamped>
    </Canvas>
  )
}
