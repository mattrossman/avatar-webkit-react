import { createContext, useContext, useEffect, useLayoutEffect, useMemo, useState, memo } from 'react'
import {
  AUPredictor,
  AUPredictorConfig,
  AUWorkerManager,
  AvatarPrediction,
  ExperimentalWorkerOptions,
} from '@quarkworks-inc/avatar-webkit'
import { InterpolationContext, InterpolationProvider } from './InterpolationContext'

type AUPredictorCallback = (results: AvatarPrediction) => void

type AUPredictorContextValue = {
  listeners: Set<AUPredictorCallback>
}

type Props = {
  children: any
  apiToken: string
  options?: Partial<ExperimentalWorkerOptions>
  stream?: MediaStream
  running?: boolean
}

const AUPredictorContext = createContext<AUPredictorContextValue | null>(null)

export function AUPredictorProvider({ children, apiToken, options, stream, running }: Props) {
  const [listeners] = useState(() => new Set<AUPredictorCallback>())
  const [activeLoops] = useState(() => new Set<Symbol>())

  const predictor = useMemo(
    () => new AUWorkerManager(apiToken, options ?? {}),
    [
      apiToken,
      options?.debug,
      options?.processHeight,
      options?.processWidth,
      options?.refineLandmarks,
      options?.resourcePaths,
      options?.shouldMirrorOutput,
    ]
  )

  const [videoEl] = useState(() => {
    const videoEl = document.createElement('video')
    videoEl.autoplay = true
    document.body.appendChild(videoEl)
    return videoEl
  })

  useLayoutEffect(() => {
    const track = stream?.getTracks()[0]
    if (!track) return

    const { width, height } = track.getSettings()
    if (!width || !height) return

    videoEl.width = width
    videoEl.height = height

    videoEl.srcObject = stream
  }, [stream])

  useEffect(() => {
    const id = Symbol()

    const loop = async () => {
      if (!activeLoops.has(id)) return

      await predictor.initialize()
      const results = await predictor.predict(videoEl)
      results && listeners.forEach((fn) => fn(results))

      loop()
    }

    if (running) {
      activeLoops.add(id)
      loop()
    }

    return () => void activeLoops.delete(id)
  }, [running, predictor, videoEl])

  return <AUPredictorContext.Provider value={{ listeners }}>{children}</AUPredictorContext.Provider>
}

export function useAvatarPrediction(fn: (results: AvatarPrediction) => void) {
  const context = useContext(AUPredictorContext)
  if (context === null) {
    throw new Error('useAvatarPrediction must be within AUPredictorProvider')
  }
  useEffect(() => {
    context.listeners.add(fn)
    return () => void context.listeners.delete(fn)
  }, [fn])
}

function DampingManager() {
  const context = useContext(InterpolationContext)
  useAvatarPrediction((results) => context?.updateTarget(results))
  return null
}

type DampedProps = {
  lambda: number
} & Props

export function AUPredictorProviderDamped({ children, lambda, ...props }: DampedProps) {
  return (
    <InterpolationProvider lambda={lambda}>
      <AUPredictorProvider {...props}>
        {children}
        <DampingManager />
      </AUPredictorProvider>
    </InterpolationProvider>
  )
}

export function useAvatarPredictionDamped(fn: (results: AvatarPrediction) => void) {
  const context = useContext(InterpolationContext)
  if (!context) throw new Error('useAvatarPredictionDamped must be within InterpolationContext')
  useLayoutEffect(() => {
    context.listeners.add(fn)
    return () => void context.listeners.delete(fn)
  }, [fn])
}
