import { AvatarPrediction, BlendShapeFactory } from '@quarkworks-inc/avatar-webkit'
import { useFrame } from '@react-three/fiber'
import { createContext, useContext, useRef, useState } from 'react'
import { MathUtils } from 'three'

type InterpolationContextValue = {
  interpolated: AvatarPrediction
  updateTarget: (results: AvatarPrediction) => void
  listeners: Set<(results: AvatarPrediction) => void>
}

export const InterpolationContext = createContext<InterpolationContextValue | null>(null)

export function InterpolationProvider({ children, lambda }: { children: any, lambda: number }) {
  const target = useRef<AvatarPrediction>()
  const [interpolated] = useState<AvatarPrediction>(() => ({
    blendShapes: BlendShapeFactory.create(),
    rotation: {},
    transform: {},
  }))
  const [listeners] = useState(() => new Set<(results: AvatarPrediction) => void>)

  useFrame((_, delta) => {
    if (!target.current) return

    for (const key0 in interpolated) {
      const valuesTarget = (target.current[key0 as keyof AvatarPrediction]) as { [key: string]: any }
      const valuesInterpolated = interpolated[key0 as keyof AvatarPrediction] as { [key: string]: number }
      for (const key1 in valuesInterpolated) {
        const valueTarget = valuesTarget[key1]
        const valueInterpolated = valuesInterpolated[key1]
        valuesInterpolated[key1] = MathUtils.damp(valueInterpolated, valueTarget, lambda, delta)
      }
    }

    listeners.forEach(fn => fn(interpolated))
  })

  const updateTarget = (results: AvatarPrediction) => {
    if (!target.current) {
      Object.assign(interpolated.blendShapes, results.blendShapes)
      Object.assign(interpolated.rotation, results.rotation)
      Object.assign(interpolated.transform, results.transform)
    }
    target.current = results
  }
  return <InterpolationContext.Provider value={{ interpolated, updateTarget, listeners }} children={children} />
}
