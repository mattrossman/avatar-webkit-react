import { useEffect, useLayoutEffect, useState } from 'react'
import { Group } from 'three'
import { Model, modelFactory, ModelSettingType } from '@quarkworks-inc/avatar-webkit-rendering'
import { useAvatarPredictionDamped } from '../context/PredictorContext'

type Props = Omit<JSX.IntrinsicElements['group'], 'args'> & {
  eyeColor?: string
  faceColor?: string
}
export function EmojiAvatar({ eyeColor, faceColor, ...props }: Props) {
  const [model, setModel] = useState<Model>()
  const [fallback] = useState(() => new Group())

  const loadModel = async () => {
    const model = await modelFactory('emoji')
    setModel(model)
  }

  useEffect(() => {
    loadModel()
  }, [])

  useLayoutEffect(() => {
    if (model) {
      model.settings = {
        eyeColor: { name: 'eyeColor', type: ModelSettingType.color, value: eyeColor },
        faceColor: { name: 'faceColor', type: ModelSettingType.color, value: faceColor },
      }
    }
  }, [eyeColor, faceColor, model])

  useAvatarPredictionDamped((results) => {
    model?.updateFromResults(results)
  })
  return (
    <group {...props}>
      <primitive object={model?.root ?? fallback} />
    </group>
  )
}
