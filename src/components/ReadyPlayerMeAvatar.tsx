import { useEffect, useState } from 'react'
import { Group } from 'three'
import { Model, modelFactory } from '@quarkworks-inc/avatar-webkit-rendering'
import { useAvatarPredictionDamped } from '../context/PredictorContext'

type Props = Omit<JSX.IntrinsicElements['group'], 'args'> & {
  url: string
}
export function ReadyPlayerMeAvatar({ url, ...props }: Props) {
  const [model, setModel] = useState<Model>()
  const [fallback] = useState(() => new Group())

  const loadModel = async (url: string) => {
    const model = await modelFactory('readyPlayerMe', url)
    setModel(model)
  }

  useEffect(() => {
    loadModel(url)
  }, [url])

  useAvatarPredictionDamped((results) => {
    model?.updateFromResults(results)
  })
  return (
    <group {...props}>
      <primitive object={model?.root ?? fallback} />
    </group>
  )
}
