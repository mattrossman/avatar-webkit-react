/// <reference types="@react-three/fiber" />
export { AUPredictorProvider, useAvatarPrediction } from './context/PredictorContext'
export { EmojiAvatar } from './components/EmojiAvatar'
export { ReadyPlayerMeAvatar } from './components/ReadyPlayerMeAvatar'
export { MozillaAvatar } from './components/MozillaAvatar'
