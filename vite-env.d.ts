/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_AVATAR_WEBKIT_AUTH_TOKEN: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
