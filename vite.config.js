import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'

process.env = {
  ...process.env,
  VITE_AVATAR_WEBKIT_AUTH_TOKEN: process.env.AVATAR_WEBKIT_AUTH_TOKEN,
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  publicDir: 'demo/public',
  build: {
    lib: {
      formats: ['cjs', 'es'],
      entry: path.resolve(__dirname, 'src/index.ts'),
      fileName: (format) => `index.${format}.js`,
    },
    rollupOptions: {
      external: [
        'three',
        'react',
        '@react-three/fiber',
        '@quarkworks-inc/avatar-webkit',
        '@quarkworks-inc/avatar-webkit-rendering',
      ],
    },
  },
})
